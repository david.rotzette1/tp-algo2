Série S01
=========
Récupérer les sources *.java dans un package s01.

Les sources de BTree/BTreeItr sont incomplets (implémentation à
terminer dans l'exercice 3).

Pour tester l'ex. 1 (ExoBTree.java) avec une version "sûre" de 
BTree/BTreeItr, un code compilé est fourni (librairie 
btree.jar, package btree) : 

  clic droit sur btree.jar -> Add as library...
Ou bien :
  File -> Project Structure -> Modules -> Dependencies -> + -> Library 
       -> New Library -> btree.jar
 
Cette librairie est aussi utilisée par le programme test BTreeTestJU
(Run As... JUnit Test Case). Il faut ajouter la librairie JUnit4 au projet :

  clic sur ligne "import...junit..." -> Alt-Enter -> Add JUnit4

------

[Sur Eclipse : 
 Project -> Properties -> Build Path -> Add External Jar... -> btree.jar
 Project -> Properties -> Build Path -> Add Library -> JUnit -> JUnit4 ]
